;;; simplephpunit.el --- run PHPUnit tests for either the current file, or the entire project

;; Author: Stephen McNelly <stephenmcnelly@gmail.com>
;; URL: http://gitlab.com/smcn/simplephpunit.el
;; Version: 0.0.1
;; Keywords: php, phpunit

;; Package-Requires: ((f "0.20.0"))

;; This file is NOT part of GNU Emacs.

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; (define-key web-mode-map (kbd "C-x f") 'simplephpunit-test-file)
;; (define-key web-mode-map (kbd "C-x p") 'simplephpunit-test-project)

;;; Code:

(require 'f)

(defun remove-nth-element (nth list)
  "Remove a specifier element (NTH) from a LIST.
Code found here:
https://emacs.stackexchange.com/questions/29786/how-to-remove-delete-nth-element-of-a-list/29791#29791"
  (if (zerop nth) (cdr list)
    (let ((last (nthcdr (1- nth) list)))
      (setcdr last (cddr last))
      list)))

(defun go-up-dir (dir)
  "Go up a directory.  Same as DIR/.."
  (setq list (split-string dir "/"))
  (string-join (remove-nth-element (- (length list) 2) list) "/"))

(defun recursively-check-for-file (file path)
  "Check PATH for FILE.  If not found, go up a directory and check again.  Return path to FILE if found."
  (catch 'not-found
    (when (equal (length (split-string path "/")) 2)
      (throw 'not-found "Error: Not found")))

  (if (f-file? (concat path file))
      (prin1 (concat path file))
    (recursively-check-for-file file (go-up-dir path))))

(defun recursively-check-for-dir (dir path)
  "Check PATH for DIR.  If not found, go up a directory and check again.  Return path to DIR if found."
  (catch 'not-found
    (when (equal (length (split-string dir "/")) 2)
      (throw 'not-found "Error: Not found")))

  (if (f-directory? (concat path dir))
      (prin1 (concat path dir))
    (recursively-check-for-dir dir (go-up-dir path))))

(defun simplephpunit-test-project ()
  "Run PHPUnit on the current project."
  (interactive)
  (async-shell-command (concat (format "php %s" (recursively-check-for-file "bin/phpunit" default-directory))
			       (format " -c %s" (recursively-check-for-file "phpunit.xml" default-directory)))))

(defun simplephpunit-test-file ()
  "Run PHPUnit on the current file."
  (interactive)
  (async-shell-command (concat (format "php %s" (recursively-check-for-file "bin/phpunit" default-directory))
			       (format " -c %s" (recursively-check-for-file "phpunit.xml" default-directory))
			       " "
			       (buffer-file-name))))

(provide 'simplephpunit)
;;; simplephpunit.el ends here
